import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import random

# komentarz do podpunktu b)
#  d0 zmienia rozmiar przeszkody co powoduje zmianę wielkości koloru odchodzącego od przeszkód,
#  ko zmienia siłę odpychania przeszkody,
#  kp zmienia przyciaganie punktu docelowego

ko=2
d0=40
kp=0.025
dr = 5



def Distance(start, finish):     # odległość pomiędzy punktami
    return np.sqrt((finish[0] - start[0]) ** 2 + (finish[1] - start[1]) ** 2)



def Up(q, qk):           # potencjał przyciągający
    return 0.5 * kp * (Distance(q, qk) ** 2)


def Fp(q, qk):   #  siła przyciągająca

    return (kp * Distance(q, qk))-0.5


def Vo(q, qo):   #  potencjał odpychający od przeszkody
    if Distance(q, qo) > d0:
        return 0
    return 0.5 * (ko * (1 / Distance(q, qo) - 1 / d0) ** 2)


def Fo(q, qo):   # Siła odpychająca od przeszkody
    # if Distance(q, qo) > dr:
    #     return 0
    # if Distance(q, qo) == 0:
    #     return 10
    return ko * (1 / Distance(q, qo) - 1 / d0) * (1 / (Distance(q, qo) ** 2))



def TableFo(q, tp):       #  sił odpychające od tablicy przeszkód
    TableFp = 0
    for p in tp:
        TableFp += (Fo(q, p))
    return TableFp


def Vec(q1, q2):    #  wektor 2 punktów
    return (q2[0] - q1[0], q2[1] - q1[1])







delta = 0.1
x_size = 10
y_size = 10
obst_pos = [(random.randint(-10, 10), random.randint(-10, 10)),
             (random.randint(-10, 10), random.randint(-10, 10)),
             (random.randint(-10, 10), random.randint(-10, 10)),
             (random.randint(-10, 10), random.randint(-10, 10))]
start_y = random.randint(-10, 10)
#start_point = (-10, start_y)

start_point = [-10, random.randint(-10, 10)]
finish_point = [10, random.randint(-10,10)]
print("Start: " + str(start_point))
print("Finish: " + str(finish_point))

x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
Z = np.exp(-X ** 0)
TableFp = TableFo(start_point, obst_pos)
curPos = start_point


n = 0
for pointx in x:
    for pointy in y:

        zpointy = int(pointy / delta + 1 / delta * 10)
        zpointx = int(pointx / delta + 1 / delta * 10)
        Z[zpointy, zpointx] = TableFo((pointx, pointy), obst_pos) + Fp((pointx, pointy), finish_point)
#
# for i in range(len(x)):
#     for j in range(len(y)):
#         sum = 0.0
#         for z in range(len(obst_pos)):
#             sum += Fo([i,j],obst_pos[z])
#         Z[i][j] = Fp([i,j],finish_point)+sum

fig = plt.figure(figsize=(10, 10))

ax = fig.add_subplot(111)
ax.set_title('metoda potencjalow')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size],
           vmax=2, vmin=-1)

plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_pos:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')

plt.colorbar(orientation='vertical')
# obliczanie i rysowanie ścieżki
while (-10 <= curPos[0] < 10 and -10 <= curPos[1] < 10):
    try:
        min_n = [float("inf"), curPos[0], curPos[1]]
        plt.plot(int((curPos[0] +100)/10), int((curPos[1]+100)/10) , 'or', color='blue')

        for i in range(-1, 2):
            for j in range(0, 2):
                if min_n[0] > Z[curPos[1] + i][curPos[0] + j]:
                    min_n = [Z[curPos[1] + i][curPos[0] + j], curPos[0] + j, curPos[1] + i]
            curPos[0] = min_n[1]
            curPos[1] = min_n[2]
    except:
        break


plt.grid(True)
plt.show()